import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatetoDateComponent } from './dateto-date/dateto-date.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [DatetoDateComponent],
  imports: [BrowserModule,FontAwesomeModule,FormsModule, NgbModule.forRoot(), BsDatepickerModule.forRoot()],
  entryComponents: [DatetoDateComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const customButton = createCustomElement(DatetoDateComponent, {
      injector: injector,
     });
     customElements.define('app-dateto-date', customButton);
  }

  ngDoBootstrap() {}
}
