import { Component, OnInit, EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dateto-date',
  template: `
  <input 
  #drp="bsDaterangepicker" 
  [maxDate]='today' 
  placement="right"
  bsDaterangepicker
  [(ngModel)]="bsRangeValue" 
  (ngModelChange)="getchange(bsRangeValue[0],bsRangeValue[1])"
  [bsConfig]="{containerClass: 'theme-dark-blue'}" >
`,
styles: [
  `.form-control{
    width: 100%;

}`
],
encapsulation: ViewEncapsulation.Native
})
export class DatetoDateComponent implements OnInit {
  datetodate:any;
  bsRangeValue: Date[];
  bsValue = new Date();
  maxDate = new Date();
  today = new Date();
  start:any;
  end:any
  @Output() DateOutput = new EventEmitter<string>();
  constructor() { 
  
  }

  ngOnInit() {
    this.bsRangeValue=[this.bsValue,this.maxDate]
    this.start=this.bsRangeValue[0];
    this.end=this.bsRangeValue[1];
    
    
  }

  getchange(start,end){
    var yyyy = start.getFullYear().toString();
    var mm = (start.getMonth()+1).toString();
    var dd  = start.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    var yyyye = end.getFullYear().toString();
    var mme = (end.getMonth()+1).toString();
    var dde  = end.getDate().toString();

    var mmeChars = mme.split('');
    var ddeChars = dde.split('');
    
      this.datetodate = (mmChars[1]?mm:"0"+mmChars[0]) + '/' + (ddChars[1]?dd:"0"+ddChars[0]) + '/' + yyyy+'-'+
 (mmeChars[1]?mme:"0"+mmeChars[0]) + '/' + (ddeChars[1]?dde:"0"+ddeChars[0]) + '/' + yyyye;
  this.DateOutput.emit(this.datetodate);
}

}
