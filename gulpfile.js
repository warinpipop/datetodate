var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gzip = require('gulp-gzip');

gulp.task('js-build', function(){
    return gulp.src([
        './dist/angular-elements/runtime.js',
        './dist/angular-elements/polyfills.js',
        './dist/angular-elements/scripts.js',
        './dist/angular-elements/main.js',
    ])
        .pipe(gp_concat('Datetodate.js'))
        .pipe(gulp.dest('Datetodate'))
      
});

gulp.task('default', ['js-build'], function(){});